import React, {Component} from "react";
import TodoEditTask from "../todo-edit-task";
import "./todo-list-item.css";

export default class TodoListItem extends Component {

    constructor() {
        super();

        this.state = {
            edited: false,
        };

        this.onEdited = () => {
            this.setState({
                edited: true,
            });
        };

        this.onEditedCancel = () => {
            this.setState({
                edited: false,
            });
        };

    }

    render() {
        const {id, label, done, important, onDeleted, onToggleImportant, onToggleDone, onEditTaskLabel} = this.props;
        const { edited } = this.state;

        let classNames = 'task-content';
        classNames += done ? ' task-done' : '';
        classNames += important ? ' task-important' : '';

        if (!edited) {
            return (
                <React.Fragment>
                    <div className={classNames} ><span  onClick={onToggleDone} >{label}</span></div>
                    <div className="task-controls btn-group ms-2">
                        <button type="button" className="btn btn-outline-primary" onClick={onToggleImportant}>
                            <i className="fa fa-exclamation"> </i>
                        </button>
                        <button type="button" className="btn btn-outline-dark" onClick={this.onEdited}>
                            <i className="fa fa-pencil"> </i>
                        </button>
                        <button type="button" className="btn btn-outline-danger"
                                onClick={onDeleted}
                        >
                            <i className="fa fa-ban"> </i>
                        </button>
                    </div>
                </React.Fragment>
            );
        }
        return <TodoEditTask
            onEditedCancel={this.onEditedCancel}
            label={label}
            id={id}
            onEditTaskLabel={onEditTaskLabel}
        />
    }




}



