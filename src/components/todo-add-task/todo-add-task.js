import React, {Component} from 'react';

import './todo-add-task.css';

export default class TodoAddTask extends Component {

    constructor() {
        super();
        this.state = {
            label: '',
        };

        this.onLabelChange = (evt) => {
            this.setState({
                label: evt.target.value
            });
        };

        this.onSubmit = (evt) => {
            evt.preventDefault();
            this.props.onItemAdded(this.state.label);
            this.setState({
                label: '',
            })
        }
    }

    render() {

        return (
            <form className="form-add-task" onSubmit={this.onSubmit}>
                <input type="text"
                       className="form-control input-add-task"
                       placeholder="Новая задача"
                       onChange={this.onLabelChange}
                       value={this.state.label}
                />
                <button className="btn btn-outline-dark btn-add-task ms-2" type="submit">
                    <i className="fa fa-plus" aria-hidden="true"> </i>
                </button>
            </form>
        );
    }
};
