import React from "react";

import './todo-task-completed.css';

const TodoTaskCompleted = ({todoData}) => {


    const completedTasksCount = todoData.filter((item) => {
        return item.done;
    }).length;
    const todoTasksCount = todoData.length - completedTasksCount;

    return (
        <p>Завершено <b>{completedTasksCount}</b>, осталось <b>{todoTasksCount}</b></p>
    );
};

export default TodoTaskCompleted;