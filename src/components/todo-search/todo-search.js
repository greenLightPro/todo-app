import React, {Component} from "react";
import './todo-search.css';

export default class TodoSearch extends Component {

    constructor() {
        super();

        this.state = {
            search: '',
        }

        this.changeSearch = (evt) => {
            const term = evt.target.value;
            this.setState(({search}) => {
                return {
                    search: term
                }
            });

            this.onSubmit = (evt) => {
                evt.preventDefault();
                this.props.onChangeSearch(this.state.search);
            }
        }
    }

    render() {
        const {search} = this.state;
        return (
            <form className="input-group ms-md-2 mt-md-0 mt-2 search-block" onSubmit={this.onSubmit}>
                <input type="text" className="form-control" placeholder="Введите текст"
                       value={search}
                       onChange={this.changeSearch}
                />
                <button className="btn btn-outline-dark">Поиск</button>
            </form>
        );
    }

};

