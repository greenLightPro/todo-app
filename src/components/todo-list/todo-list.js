import React from "react";
import TodoListItem from "../todo-list-item";
import './todo-list.css';



const TodoList = ( {
                       todoData,
                       onDeleted,
                       onToggleImportant,
                       onToggleDone, onEditTaskLabel
                   } ) => {

    const todoItems = todoData.map((item) => {

        const { id } = item;

        return (
            <div className="list-group-item task-item" key={id} >
                <TodoListItem
                    { ...item }
                    onDeleted={() => onDeleted(id)}
                    onToggleImportant={() => onToggleImportant(id)}
                    onToggleDone={() => onToggleDone(id)}
                    onEditTaskLabel={onEditTaskLabel}

                />
            </div>

        );
    });

    if (todoItems.length > 0) {
        return (
            <div className="list-group task-list">
                {todoItems}
            </div>
        );
    }
    return <p>Задач нет</p>


};

export default TodoList;