import React, {Component} from 'react';


export default class TodoFilter extends Component {

    constructor() {
        super();

        this.buttons = [
            {
                name: 'all',
                label: 'Все',
            },
            {
                name: 'active',
                label: 'Активные',
            },
            {
                name: 'done',
                label: 'Выполненные',
            },
        ];
    }

    render() {

        const {filterName, onChangedFilter} = this.props;


        const buttons = this.buttons.map(({name, label}) => {
            const btnClass = filterName === name ? 'btn-primary' : 'btn-outline-secondary';

            return (
                <button type="button"
                        key={name}
                        className={`btn ${btnClass}`}
                        onClick={() => onChangedFilter(name)}>
                {label}
            </button>);
        });
        return (
            <div className="btn-group">
                { buttons }
            </div>
        );
    }

}
