import React from 'react';

import './todo-search-text.css'

const TodoSearchText = ({term}) => {
    if (term !== '') {
        return  <p className="mt-4 mb-0">Результат поиска по запросу <b>"{term}"</b></p>
    }

    return false;
}

export default TodoSearchText;