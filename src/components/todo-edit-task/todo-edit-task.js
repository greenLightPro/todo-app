import React, {Component} from "react";
import "./todo-edit-task.css";

export default class TodoEditTask extends Component {

    constructor() {
        super();


        this.state = {
            label:'',
            modified: false,
        };

        this.onChange = (evt) => {
            const label = evt.target.value;
            this.setState({
                modified: true,
                label,
            });
        };
    }

    render() {

        const { id, onEditedCancel, label: propsLabel, onEditTaskLabel } = this.props;
        const { modified, label: stateLabel } = this.state;
        const labelText = modified ? stateLabel : propsLabel;

        const onSubmit = (evt) => {
            evt.preventDefault();
            const value = modified ? stateLabel : propsLabel;
            onEditTaskLabel(id, value);
            onEditedCancel();
        }

        return (
            <form className="task-edit-form" onSubmit={ onSubmit }>
                <div className="task-content">
                    <input type="text" name="edit-task" className="form-control input-edit-task"
                           value={labelText}
                           onChange={this.onChange}
                    />
                </div>
                <div className="task-controls btn-group ms-2">
                    <button type="submit" className="btn btn-outline-success" >
                        <i className="fa fa-check" aria-hidden="true"> </i>
                    </button>
                </div>
            </form>
        );
    }
}
