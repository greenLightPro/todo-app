import React from 'react';

import './app-header.css';

const AppHeader = () => {
    return (
        <h2>Планировщик задач</h2>
    );
};

export default AppHeader;

