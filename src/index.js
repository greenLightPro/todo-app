import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TodoList from "./components/todo-list";
import TodoFilter from "./components/todo-filter";
import TodoAddTask from "./components/todo-add-task";
import TodoSearch from "./components/todo-search";
import TodoTaskCompleted from "./components/todo-task-completed";
import AppHeader from "./components/app-header";
import TodoSearchText from "./components/todo-search-text";




const todoData = [
    {
        label: 'Открыть планировщик задач',
        done: true,
        important: false,
        id: 1,
    },
    {
        label: 'Выпить чашку горячего шоколада',
        done: false,
        important: false,
        id: 2,
    },
    {
        label: 'Поделиться хорошим настроением с окружающими',
        done: false,
        important: false,
        id: 3,
    },
];

class App extends Component {

    constructor() {
        super();

        this.state = {
            todoData: todoData,
            filter: 'all', //active, done
            term: '',
        }

        this.deleteItem = (id) => {

            this.setState(({ todoData }) => {
                const idx = todoData.findIndex((item) => {
                    return item.id === id;
                });
                const newTodoData = [
                        ...todoData.slice(0, idx),
                        ...todoData.slice(idx +1),
                ];
                return {
                    todoData: newTodoData,
                };
            });
        }

        this.addItem = (text) => {
            this.setState(({ todoData }) => {
                const newId = todoData.length > 0 ? todoData[todoData.length-1].id + 1 : 0;

                const newTodoData = [
                    ...todoData,
                    {
                        label: text,
                        done: false,
                        important: false,
                        id: newId,
                    },
                ];
                return (
                    {
                        todoData: newTodoData,
                    }
                );
            });
        }

        this.toggleImportant = (id) => {
            this.setState(({todoData}) => {
                return {
                    todoData: this.toggleProperty(todoData, id, 'important'),
                };
            });
        }

        this.toggleDone = (id) => {
            this.setState(({todoData}) => {
                return {
                    todoData: this.toggleProperty(todoData, id, 'done'),
                };
            });
        }

        this.editTaskLabel = (id, value) => {
            this.setState(({todoData}) => {
                return {
                    todoData: this.changeProperty(todoData, id, 'label', value),
                };
            });
        }

        this.getSearchItems = (items, term) => {
            if (term === '') {
                return items;
            }
            return items.filter((item) => {
                return item.label
                    .toLowerCase()
                    .indexOf(term.toLowerCase()) !== -1
            })
        }

        this.getFilterItems = (items, filterName) => {
            switch (filterName) {
                case 'all':
                    return items;
                case 'done':
                    return items.filter((item) => item.done);
                case 'active':
                    return items.filter((item) => !item.done);
                default:
                    return items;
            }

        }

        this.changeSearch = (term) => {
            this.setState({
                term,
            });
        }

        this.changeFilter = (filter) => {
            this.setState({
                filter,
            });
        }

    }

    changeProperty(arr, id, propName, value) {
        const idx = arr.findIndex((item) => item.id === id);
        const oldItem = arr[idx];
        const newItem = {
            ...oldItem,
            [propName] : value,
        };

        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1),
        ];
    }

    toggleProperty(arr, id, propName) {
        const idx = arr.findIndex((item) => item.id === id);
        const oldItem = arr[idx];
        const newItem = {
            ...oldItem,
            [propName]: !oldItem[propName],
        };

        return [
                ...arr.slice(0, idx),
                newItem,
                ...arr.slice(idx+1)
            ];
    }



    render() {
        const {todoData, filter, term} = this.state;

        const visibleItems = this.getFilterItems(this.getSearchItems(todoData, term), filter);


        return (
            <section className="todo-outer-block">
                <div className="container task-container">
                    <div className="todo-outer py-4">

                        <AppHeader />

                        <div className="todo-wrap mt-4">
                            <TodoTaskCompleted todoData = {todoData} />
                            <div className="todo-header d-block d-md-flex flex-row justify-content-between align-items-start">
                                <TodoFilter
                                    onChangedFilter={this.changeFilter}
                                    filterName={filter}
                                />
                                <TodoSearch  onChangeSearch={this.changeSearch} />
                            </div>
                            <TodoSearchText term={term} />
                            <div className="todo-body mt-4">
                                <TodoList
                                    todoData = {visibleItems}
                                    onDeleted = {this.deleteItem}
                                    onToggleImportant = {this.toggleImportant}
                                    onToggleDone = {this.toggleDone}
                                    onEditTaskLabel = {this.editTaskLabel}
                                />
                            </div>
                            <div className="todo-footer mt-4 input-group">
                                <TodoAddTask onItemAdded = {this.addItem} />
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        );
    }

}



ReactDOM.render(<App />, document.getElementById('root'));